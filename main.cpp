#include <bits/stdc++.h>

using namespace std;

void DFS(int searchpoint, vector<int> & visitedpreviously, int & currentmax, const vector<vector<int>> edges)
{
    visitedpreviously.push_back(searchpoint);

for(int i =0;i<edges.size();i++)
{
if (edges[i][0]==searchpoint)
{
int connectedpoint = edges[i][1];
auto searchresoult = find(visitedpreviously.begin(),visitedpreviously.end(),connectedpoint);
if (searchresoult==visitedpreviously.end())
{
currentmax++;
DFS(connectedpoint,visitedpreviously,currentmax,edges);
}

}
}
}


int maxRegion(vector<vector<int>> grid)
{
int maxregion;
int rowsize = grid[0].size();
vector<int> pointflat;
for(int row =0;row<grid.size();row++ )
{
    for (int collumn = 0; collumn<grid[row].size();collumn++)
    {
    pointflat.push_back(grid[row][collumn]);
    }
}
vector <vector<int>> edges;

// edges

for (int i=0;i<pointflat.size();i++)
{
if (pointflat[i]==0)
continue;
else
{
//dolny rzad
if(i-rowsize>=0)
{
if (i%rowsize>0)
if (pointflat[i-rowsize-1]==1)
edges.push_back({i,i-rowsize-1});



if (pointflat[i-rowsize]==1)
edges.push_back({i,i-rowsize});

if ((i%rowsize!=(rowsize-1)))
if (pointflat[i-rowsize+1]==1)
edges.push_back({i,i-rowsize+1});

}


if (i%rowsize>0)
if (pointflat[i-1]==1)
edges.push_back({i,i-1});

if ((i%rowsize!=(rowsize-1)))
if (pointflat[i+1]==1)
edges.push_back({i,i+1});




// gorny rza
if (i+rowsize<pointflat.size())
{
if (i%rowsize>0)
if (pointflat[i+rowsize-1]==1)
    edges.push_back({i,i+rowsize-1});

if (pointflat[i+rowsize]==1)
edges.push_back({i,i+rowsize});

if ((i%rowsize!=(rowsize-1)))
if (pointflat[i+rowsize+1]==1)
edges.push_back({i,i+rowsize+1});
}
}
}


vector<int>startingpoints;
for (uint i = 0 ; i<pointflat.size(); i++)
{
if(pointflat[i]==1)
{
    startingpoints.push_back(i);
}
}
if(startingpoints.size()>0)
maxregion=1;
else
{
maxregion=0;
return maxregion;
}

for (auto start = startingpoints.begin();start!=startingpoints.end();start++)
{
std::vector<int> visitedpoints{*start};
int searchpoint = *start;
int currentmax = 1;
DFS(searchpoint,visitedpoints,currentmax,edges);
if (currentmax>maxregion)
{
    maxregion=currentmax;
}



}


return maxregion;


}







int main()
{
    ofstream fout(getenv("OUTPUT_PATH"));

    int n;
    cin >> n;
    cin.ignore(numeric_limits<streamsize>::max(), '\n');

    int m;
    cin >> m;
    cin.ignore(numeric_limits<streamsize>::max(), '\n');

    vector<vector<int>> grid(n);
    for (int i = 0; i < n; i++) {
        grid[i].resize(m);

        for (int j = 0; j < m; j++) {
            cin >> grid[i][j];
        }

        cin.ignore(numeric_limits<streamsize>::max(), '\n');
    }

    int res = maxRegion(grid);

    fout << res << "\n";

    fout.close();

    return 0;
}
